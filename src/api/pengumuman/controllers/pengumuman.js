'use strict';

/**
 * pengumuman controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::pengumuman.pengumuman');
