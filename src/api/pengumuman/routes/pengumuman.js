'use strict';

/**
 * pengumuman router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::pengumuman.pengumuman');
